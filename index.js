// https://jsonplaceholder.typicode.com/todos

fetch('https://jsonplaceholder.typicode.com/todos')
.then(res => res.json())
.then(result => {
	console.log(result)
// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

	let title = result.map((title) => title.title)
	console.log(result)



})


fetch('https://jsonplaceholder.typicode.com/todos/2')
.then(res => res.json())
.then(result => {
	console.log(result)

	console.log(`This is my title: ${result.title} and this is my status: ${result.completed}`)
})

fetch('https://jsonplaceholder.typicode.com/todos',{ 
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	body:JSON.stringify({
		userId: 1,
		id: 100,
		title: "Activity s28",
		completed: true
	})
})
.then(res => res.json())
.then(result => {
	console.log(result)
})

fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PATCH',
	headers: {'Content-Type': 'application/json'},
	body: JSON.stringify({
		completed: true,
		date: "12-02-2021"
	})
})
.then(res => res.json())
.then(result => {
	console.log(result)
})

fetch('https://jsonplaceholder.typicode.com/todos/4',{
	method: 'DELETE',
})
.then(res => res.json())
.then(result => {
	console.log(result)})


